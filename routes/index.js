var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { layout: false });
});

router.get('/dashboard', function (req, res, next) {
  res.render('dashboardTable');
});
router.get('/list-cars', function (req, res, next) {
  res.render('dashboardListCars');
});
router.get('/new-cars', function (req, res, next) {
  res.render('dashboardNewCar');
});

module.exports = router;
